# Image to drawing for pix2pix

Automaticaly transform a folder of images to a compatible dataset for pix2pix.
The dataset will be store in the current directory
```bash
/datasets/you_project_name/train
/datasets/you_project_name/test
/datasets/you_project_name/val
```
You can choose the repartition of your dataset into the different folders.
You can also choose between different types of drawing like edge detection with canny, dodge or you can also fill manually the second image in a separate folder.

## Setup
```bash
git clone https://gitlab.com/turbocoding/image2drawing.git
cd image2drawing
```

## Use
```bash
python main.py
            --drawing-type edge // edge, manual or dodge
            --flip-image True // True by default, uses a mirror of each images to double the dataset. False to not mirror images
```


