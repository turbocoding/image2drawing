import argparse
import os
import scipy.misc
import numpy as np

from image2drawing import image2drawing

parser = argparse.ArgumentParser(description='')
parser.add_argument('--flip-image', dest='flip_image', type=bool, default=True, help='True by default, uses a mirror of each images to double the dataset. False to not mirror images')
parser.add_argument('--drawing-type', dest='drawing_type', default='edge', help='Type of drawing method, "edge" for auto-canny, "dodge" for dodge V2 and manual to provide manualy the second source. This need to be used in combination with --manual-folder')
parser.add_argument('--source-folder', dest='source_folder',  default='./papa', help='Path to the images manually processed. The images names must match the orignal ones.')
parser.add_argument('--manual-folder', dest='manual_folder', default='./manual', help='Path to the images manually processed. The images names must match the orignal ones.')
parser.add_argument('--datasets-dir', dest='datasets_dir', default='./datasets', help='Datasets are saved here')

args = parser.parse_args()

def main():
    if not os.path.exists(args.datasets_dir):
        os.makedirs(args.datasets_dir+"/cars/test")
        os.makedirs(args.datasets_dir+"/cars/train")
        os.makedirs(args.datasets_dir+"/cars/val")

    model = image2drawing(drawing_type=args.drawing_type, manual_folder=args.manual_folder)

    if args.flip_image == True:
        model.run()
    else:
        model.run()

if __name__ == '__main__':
    main()
