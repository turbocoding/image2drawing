# -*- coding: utf-8 -*-

import cv2
import numpy as np
import math
import glob
import ntpath
import os.path


class image2drawing(object):
  def __init__(self, drawing_type='edge', source_folder='./source', manual_folder='./manual'):
    print("Initialisation")
    # Where the final images must be saved
    self.baseFolderDataSet = "datasets/cars/"
    # Source files folder
    self.sourceFilesFolder = source_folder+"/*"
    # Set the final result image size
    self.resize_value = 256
    self.drawing_type = drawing_type
    self.manual_folder = manual_folder
    self.ignoreThisImage = False

  def folderDataSet(self, id, lenght):
    if (id > math.floor(lenght*0.95)):
      return self.baseFolderDataSet+"val/"
    elif (id > math.floor(lenght*0.70)):
      return self.baseFolderDataSet+"test/"
    else:
      return self.baseFolderDataSet+"train/"

  def auto_canny(self, image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    # return the edged image
    return edged
    
  def createCanevas(self, width, height):
    blank_file = 'blank.jpg'
    img_blank = cv2.imread(blank_file)
    return cv2.resize(img_blank, (width, height))

  def getEdgedImage(self, img_src):
    img_gray = cv2.cvtColor(img_src, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(img_gray, (3, 3), 0)
    auto = self.auto_canny(blurred)
    revert = cv2.bitwise_not(auto)
    return cv2.cvtColor(revert, cv2.COLOR_GRAY2BGR)
  
  def dodgeV2(image, mask):
    return cv2.divide(image, 255-mask, scale=256)

  def getDodgeImage(self, img_src):
    img_gray = cv2.cvtColor(img_src, cv2.COLOR_BGR2GRAY)
    img_gray_inv = 255 - img_gray
    img_blur = cv2.GaussianBlur(img_gray_inv, ksize=(21, 21),
                            sigmaX=0, sigmaY=0)
    img_blend = dodgeV2(img_gray, img_blur)
    return cv2.cvtColor(img_blend, cv2.COLOR_GRAY2BGR)

  def concatenateImages(self, canevas, leftImage, rightImage):
    canevas[0:self.resize_value, 0:self.resize_value] = leftImage
    canevas[0:self.resize_value, self.resize_value:self.resize_value+self.resize_value] = rightImage
    return canevas

  def resizeAndCropImage(self, img_src):
    img_src = cv2.resize(img_src, (1280, 1024))
    canevas = self.createCanevas(1280, 1280)
    canevas[0:1024, 0:1280] = img_src
    return cv2.resize(canevas, (self.resize_value, self.resize_value))

  def getManualImage(self, img_file, flip):
    head, tail = ntpath.split(img_file)
    img_file = self.manual_folder+"/"+tail
    if os.path.isfile(img_file):
      img_src = cv2.imread(img_file)
      if flip:
        img_src = cv2.flip(img_src, 1)
      return self.resizeAndCropImage(img_src)
    else:
      self.ignoreThisImage = True  

  def run(self):
    print("Running with drawing type "+self.drawing_type)
    files = glob.glob(self.sourceFilesFolder)
    i = 0
    totalFiles = len(files)*2

    for img_file in files:
        for j in range(2):
          self.ignoreThisImage = False
          flip = False
          i = i + 1
          print(str(i))
          # get a canevas of 2*ResizeValue * ResizeValue (default=512*256)
          canevas = self.createCanevas(2*self.resize_value, self.resize_value)
          
          img_src = cv2.imread(img_file)
          if (j==1):
            flip = True
            img_src = cv2.flip(img_src, 1)
          
          img_src = self.resizeAndCropImage(img_src)
          if self.drawing_type == 'dodge':
            img_result = self.getDodgeImage(img_src)
          elif self.drawing_type == 'manual': 
            img_result = self.getManualImage(img_file, flip)
          else:
            img_result = self.getEdgedImage(img_src)

          if not self.ignoreThisImage:
            img4training = self.concatenateImages(canevas, img_src, img_result)
            cv2.imwrite(self.folderDataSet(i, totalFiles)+str(i)+".jpg", img4training)